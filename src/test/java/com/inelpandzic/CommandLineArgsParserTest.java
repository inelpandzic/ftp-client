package com.inelpandzic;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

public class CommandLineArgsParserTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void processArgs_noArgsProvided_ExceptionThrown() {
        CommandLineArgsParser parser = new CommandLineArgsParser();
        String[] args = new String[]{};

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("No arguments provided.");

        parser.parseArgs(args);
    }

    @Test
    public void processArgs_toManyArgsProvided_ExceptionThrown() {
        CommandLineArgsParser parser = new CommandLineArgsParser();
        String[] args = new String[]{"-u", "user", "-p", "pass", "-server", "192.168.0.9", "-files", "/root/path/file", "-foo", "bar"};

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("To many (" + args.length + ") arguments provided.");

        parser.parseArgs(args);
    }

    @Test
    public void processArgs_oddNumberOfArgsProvided_ExceptionThrown() {
        CommandLineArgsParser parser = new CommandLineArgsParser();
        String[] args = new String[]{"-u", "user", "-p", "pass", "-server", "192.168.0.9", "-files"};

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Invalid (odd) number of arguments provided.");

        parser.parseArgs(args);
    }

    @Test
    public void processArgs_noFilesArgOptionProvided_ExceptionThrown() {
        CommandLineArgsParser parser = new CommandLineArgsParser();
        String[] args = new String[]{"-u", "user", "-p", "pass", "-server", "192.168.0.9"};

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Missing -files argument.");

        parser.parseArgs(args);
    }

    @Test
    public void processArgs_unknownArgSwitchProvided_ExceptionThrown() {
        CommandLineArgsParser parser = new CommandLineArgsParser();
        String[] args = new String[]{"-u", "user", "-foo", "pass", "-files", "/root/path/file"};

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Unknown (-foo) argument provided.");

        parser.parseArgs(args);
    }

    @Test
    public void processArgs_moreThan5filesProvided_ExceptionThrown() {
        CommandLineArgsParser parser = new CommandLineArgsParser();
        String[] args = new String[]{"-files", "/root/path/foo,/root/path/bar,/root/path/baz,/root/path/faz,/root/path/far,/root/path/fob"};

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Total number of files exceeded.");

        parser.parseArgs(args);
    }

    @Test
    public void getPass_noPassArgProvided_returnsDefaultPassValue() {
        CommandLineArgsParser parser = new CommandLineArgsParser();
        String[] args = new String[]{"-u", "user", "-files", "/root/path/file"};

        parser.parseArgs(args);

        Assert.assertEquals("pass", parser.getPass());
    }

    @Test
    public void getPass_passArgProvided_returnsProvidedPassValue() {
        CommandLineArgsParser parser = new CommandLineArgsParser();
        String[] args = new String[]{"-u", "user", "-p", "providedPass", "-files", "/root/path/file"};


        parser.parseArgs(args);

        Assert.assertEquals("providedPass", parser.getPass());
    }

    @Test
    public void getUser_noUserArgProvided_returnsDefaultUserValue() {
        CommandLineArgsParser parser = new CommandLineArgsParser();
        String[] args = new String[]{"-p", "pass",  "-files", "/root/path/file"};

        parser.parseArgs(args);

        Assert.assertEquals("user", parser.getUser());
    }

    @Test
    public void getUser_userArgProvided_returnsProvidedUserValue() {
        CommandLineArgsParser parser = new CommandLineArgsParser();
        String[] args = new String[]{"-u", "providedUser","-p", "pass",  "-files", "/root/path/file"};

        parser.parseArgs(args);

        Assert.assertEquals("providedUser", parser.getUser());
    }

    @Test
    public void getServer_noServerArgProvided_returnsDefaultServerValue() {
        CommandLineArgsParser parser = new CommandLineArgsParser();
        String[] args = new String[]{"-u", "user", "-files", "/root/path/file"};

        parser.parseArgs(args);

        Assert.assertEquals("127.0.0.1", parser.getServer());
    }

    @Test
    public void getServer_ServerArgProvided_returnsProvidedServerValue() {
        CommandLineArgsParser parser = new CommandLineArgsParser();
        String[] args = new String[]{"-u", "user","-p", "providedPass", "-server", "192.168.0.9", "-files", "/root/path/file"};

        parser.parseArgs(args);

        Assert.assertEquals("192.168.0.9", parser.getServer());
    }

    @Test
    public void getFiles_passFilesListStringArg_returnsListOfFilesProvided() {
        CommandLineArgsParser parser = new CommandLineArgsParser();
        String[] args = new String[]{"-server", "192.168.0.9", "-files", "/root/path/file,/root/path/foo,/root/path/bar"};

        parser.parseArgs(args);
        List<String> expected = Arrays.asList("/root/path/file", "/root/path/foo", "/root/path/bar");

        Assert.assertThat(parser.getFiles(), is(expected));
    }
}
