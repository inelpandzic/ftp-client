package com.inelpandzic;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UploadStatsHandlerTest {
    private UploadStatsHandler uploadStatsHandler;

    @Before
    public void setUp() {
        uploadStatsHandler = new UploadStatsHandler();
        uploadStatsHandler.incrementUploadedFilesCount();
        uploadStatsHandler.incrementUploadedFilesCount();
        uploadStatsHandler.incrementUploadedFilesCount();
        uploadStatsHandler.handleTotalPassedTime(50);
        uploadStatsHandler.handleAverageUploadSpeed(550);
        uploadStatsHandler.handleAverageUploadSpeed(650);
    }

    @Test
    public void calculateAverageUploadSpeed_provideNewUploadSpeed_calculatesAverageUploadSpeed() {
        uploadStatsHandler.handleAverageUploadSpeed(750);

        Assert.assertEquals(650, uploadStatsHandler.calculateAverageUploadSpeed(), 0);
    }

    @Test
    public void handleTotalPassedTime_provideNewElapsedTime_returnsLargestTimeValue() {
        uploadStatsHandler.handleTotalPassedTime(70);
        uploadStatsHandler.handleTotalPassedTime(40);

        Assert.assertEquals(70, uploadStatsHandler.getTotalElapsedTimeInSeconds(), 0);
    }
}
