package com.inelpandzic;

import java.io.File;
import java.io.IOException;

import static java.lang.System.out;

public class App {

    public static void main(String[] args) {

        CommandLineArgsParser argsParser = new CommandLineArgsParser();
        try {
            argsParser.parseArgs(args);
        } catch (IllegalArgumentException e) {
            out.println(e.getMessage() + "\r\n");
            return;
        }

        final UploadStatsHandler uploadStats = new UploadStatsHandler();

        argsParser.getFiles().parallelStream().forEach(file -> {
            try {
                FtpClient ftpClient = new FtpClient(uploadStats);
                ftpClient.connect(argsParser.getUser(), argsParser.getPass(), argsParser.getServer());
                ftpClient.upload(new File(file));
                ftpClient.disconnect();
            } catch (IOException e) {
                out.format("%-10s --- %s (%s)%n", "Failed", file, e.getMessage());
            }
        });

        out.println("---------------------------------------------------------------");
        out.format("Uploaded %d file(s) in %.0f seconds @ %.0f KB/s %n",
                uploadStats.getUploadedFilesCount(),
                uploadStats.getTotalElapsedTimeInSeconds(),
                uploadStats.calculateAverageUploadSpeed());

    }
}
