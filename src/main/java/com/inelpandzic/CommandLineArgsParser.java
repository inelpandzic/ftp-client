package com.inelpandzic;

import java.util.*;

class CommandLineArgsParser {

    private String user = "user";
    private String pass = "pass";
    private String server = "127.0.0.1";
    private List<String> files;

    void parseArgs(String[] args) throws IllegalArgumentException {

        if (args.length == 0) {
            throw new IllegalArgumentException("No arguments provided.");
        }

        if (args.length > 8) {
            throw new IllegalArgumentException("To many (" + args.length + ") arguments provided.");
        }

        if ((args.length & 1) == 1) {
            throw new IllegalArgumentException("Invalid (odd) number of arguments provided.");
        }

        if (!args[args.length - 2].equals("-files")) {
            throw new IllegalArgumentException("Missing -files argument.");
        }

        for (int i = 0; i < args.length; i += 2) {
            switch (args[i]) {
                case "-u":
                    user = args[i + 1];
                    break;
                case "-p":
                    pass = args[i + 1];
                    break;
                case "-server":
                    server = args[i + 1];
                    break;
                case "-files":
                    files = parseFiles(args[i + 1]);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown (" + args[i] + ") argument provided.");
            }
        }
    }

    String getUser() {
        return user;
    }

    String getPass() {
        return pass;
    }

    String getServer() {
        return server;
    }

    List<String> getFiles() {
        return files;
    }

    private List<String> parseFiles(String fileList) {

        StringTokenizer tokenizer = new StringTokenizer(fileList, ",");

        if (tokenizer.countTokens() > 5) {
            throw new IllegalArgumentException("Total number of files exceeded.");
        }

        List<String> files = new LinkedList<>();

        while (tokenizer.hasMoreTokens()) {
            files.add(tokenizer.nextToken());
        }

        return files;
    }
}
