package com.inelpandzic;

import java.io.*;
import java.net.Socket;
import java.util.StringTokenizer;

import static java.lang.System.out;

class FtpClient {

    final private UploadStatsHandler uploadStats;
    private Socket controlConnection;
    private BufferedReader reader;
    private BufferedWriter writer;

    FtpClient(final UploadStatsHandler uploadStats) {
        this.uploadStats = uploadStats;
    }

    void connect(String user, String pass, String server) throws IOException {
        if (controlConnection != null) {
            throw new IOException("FtpClient is already connected. Disconnect first.");
        }

        controlConnection = new Socket(server, 21);
        reader = new BufferedReader(new InputStreamReader(controlConnection.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(controlConnection.getOutputStream()));

        String response = reader.readLine();
        if (!response.startsWith("220 ")) {
            throw new IOException(
                    "FtpClient received an unknown response when connecting to the FTP server: " + response);
        }

        sendCommand("USER " + user);
        response = reader.readLine();
        if (!response.startsWith("331 ")) {
            throw new IOException(
                    "FtpClient received an unknown response after sending the user: " + response);
        }

        sendCommand("PASS " + pass);
        response = reader.readLine();
        if (!response.startsWith("230 ")) {
            throw new IOException(
                    "FtpClient was unable to log in with the supplied password: " + response);
        }
    }

    void upload(File file) throws IOException {
        if (file.isDirectory()) {
            throw new IOException("FtpClient cannot upload a directory.");
        }

        upload(new FileInputStream(file), file.getName(), file.length());
    }

    void disconnect() throws IOException {
        try {
            sendCommand("QUIT");
        } finally {
            controlConnection.close();
        }
    }

    private void upload(InputStream inputStream, String filename, long fileSize) throws IOException {

        BufferedInputStream file = new BufferedInputStream(inputStream);
        double fileSizeInKB = Math.ceil(fileSize / 1024.0);

        sendCommand("PASV");
        String response = reader.readLine();
        if (!response.startsWith("227 ")) {
            throw new IOException("FtpClient could not request passive mode: " + response);
        }

        Socket dataConnection = getDataConnection(response);

        sendCommand(resolveFileTranferType(filename));
        response = reader.readLine();
        if (!(response.startsWith("200 "))) {
            throw new IOException("FtpClient encountered problem with TYPE command:  " + response);
        }

        sendCommand("STOR " + filename);
        response = reader.readLine();
        if (!(response.startsWith("125 ") || response.startsWith("150 "))) {
            throw new IOException("FtpClient encountered problem with STOR command:  " + response);
        }

        BufferedOutputStream output = new BufferedOutputStream(dataConnection.getOutputStream());

        out.format("%-10s --- %s (%.0f KB)%n", "Uploading", filename, fileSizeInKB);
        long startTime = System.currentTimeMillis();

        byte[] buffer = new byte[4096];
        int bytesRead = 0;
        while ((bytesRead = file.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
        output.flush();
        output.close();
        file.close();

        double elapsedTimeInSeconds = Math.ceil((System.currentTimeMillis() - startTime) / 1000.0);
        double uploadSpeed = Math.ceil(fileSizeInKB / elapsedTimeInSeconds);

        response = reader.readLine();
        if (response.startsWith("226 ") || response.startsWith("250 ")) {
            out.format("%-10s --- %s: elapsed %.0f second(s), upload speed %.0f KB/s%n",
                    "Finished", filename, elapsedTimeInSeconds, uploadSpeed);

            synchronized (uploadStats) {
                uploadStats.handleAverageUploadSpeed(uploadSpeed);
                uploadStats.handleTotalPassedTime(elapsedTimeInSeconds);
                uploadStats.incrementUploadedFilesCount();
            }

        } else {
            out.format("%-20.20s === Upload failed (%s)%n", filename, response);
        }
    }

    private Socket getDataConnection(String response) throws IOException {
        StringBuilder serverIp = new StringBuilder();
        int serverPort = -1;

        int opening = response.indexOf('(');
        int closing = response.indexOf(')', opening + 1);

        if (closing > 0) {
            String dataLink = response.substring(opening + 1, closing);
            StringTokenizer tokenizer = new StringTokenizer(dataLink, ",");

            try {
                serverIp.append(tokenizer.nextToken());
                serverIp.append(".");
                serverIp.append(tokenizer.nextToken());
                serverIp.append(".");
                serverIp.append(tokenizer.nextToken());
                serverIp.append(".");
                serverIp.append(tokenizer.nextToken());

                serverPort = Integer.parseInt(tokenizer.nextToken()) * 256 + Integer.parseInt(tokenizer.nextToken());
            } catch (Exception e) {
                throw new IOException("FtpClient received bad data connection information: " + response);
            }
        }

        return new Socket(serverIp.toString(), serverPort);
    }

    private void sendCommand(String command) throws IOException {
        try {
            writer.write(command + "\r\n");
            writer.flush();
        } catch (IOException e) {
            throw e;
        }
    }

    private String resolveFileTranferType(String filename){
        String extension = filename.substring(filename.lastIndexOf('.') + 1);
        return (extension.equals("ascii") || extension.equals("dat") || extension.equals("txt")) ? "TYPE A" : "TYPE I";
    }
}
