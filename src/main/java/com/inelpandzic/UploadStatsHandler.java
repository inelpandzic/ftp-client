package com.inelpandzic;

import java.util.LinkedList;
import java.util.List;

class UploadStatsHandler {
    private byte uploadedFilesCount;
    private double totalElapsedTimeInSeconds;
    private List<Double> UploadSpeedValues = new LinkedList<>();

    void incrementUploadedFilesCount() {
        uploadedFilesCount++;
    }

    void handleTotalPassedTime(double elapsedTimeInSeconds) {
        if (elapsedTimeInSeconds > totalElapsedTimeInSeconds) totalElapsedTimeInSeconds = elapsedTimeInSeconds;
    }

    void handleAverageUploadSpeed(double uploadSpeedInKBs) {
        UploadSpeedValues.add(uploadSpeedInKBs);
    }

    double getTotalElapsedTimeInSeconds() {
        return totalElapsedTimeInSeconds;
    }

    int getUploadedFilesCount() {
        return uploadedFilesCount;
    }

    double calculateAverageUploadSpeed() {
        return UploadSpeedValues.stream().mapToDouble(i -> i).sum() / uploadedFilesCount;
    }
}
